<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

function table($table)
{
    return DB::table($table);
}

function getUser()
{
    return auth()->user();
}

function setResponse($status, $data, $code = 200, $message = 'Berhasil Ambil Data')
{
    return response()->json([
        'status' => $status,
        'message' => $message,
        'result' => $data
    ], $code);
}

// Singkat Nama
function singkat_nama($nama)
{
    if (preg_match_all('/\b(\w)/', strtoupper($nama), $m)) {
        return $v = implode('', $m[1]); // RP
    } else {
        return $nama;
    }
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function dateInputFormat($date, $status)
{
    if ($status == 'awal') {
        $code = ' 00:00:00';
    } else {
        $code = ' 23:59:59';
    }
    return Carbon::parse($date)->format('Y-m-d') . $code;
}

function grafikByWeek($array)
{
    $grafik = $array->pluck('total', 'week')->toArray() + array_fill(1, 7, 0);
    ksort($grafik);

    $arrlength = count($grafik);
    for ($x = 1; $x < $arrlength + 1; $x++) {
        $newArray[$x] = (int)$grafik[$x];
    }

    return $newArray;
}

function getAllWeekName()
{
    return [
        'Minggu', 'Senin', 'Selesa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'
    ];
}

function formatGrafik($array)
{
    return array_map('intval', explode(',', implode(',', $array)));
}

function grafikByMonthGroupWeek($array)
{
    $countWeek = Carbon::now()->endOfMonth()->weekOfMonth;
    $grafik = $array->pluck('total', 'week')->toArray() + array_fill(1, $countWeek, 0);
    ksort($grafik);

    $arrlength = count($grafik);
    for ($x = 1; $x < $arrlength + 1; $x++) {
        $newArray[$x] = (int)$grafik[$x];
    }

    return $newArray;
}

function getAllQuarterWeekName()
{
    $countWeek = Carbon::now()->endOfMonth()->weekOfMonth;
    $array = [];
    for ($i = 1; $i < $countWeek + 1; $i++) {
        $array[] = 'Minggu ' . $i;
    }
    return $array;
}

function grafikByMonthApi($array, $bulanSekarang, $bulanSebelumnya)
{
    $now = intval($bulanSekarang);
    $before = intval($bulanSebelumnya);
    $beforeChange = $before == 12 ? 0 : $before;
    $grafik = collect($array->pluck('total', 'month')->toArray() + array_fill(1, 12, 0))->sortKeys();
    if ($before <= 7 || $beforeChange == 0) {
        return $slice = collect(array_slice($grafik->toArray(), $beforeChange, 5))->sortKeys()->toArray();
    } else {
        $array1 = collect(array_slice($grafik->toArray(), $beforeChange, 5))->sortKeys()->toArray();
        $array2 = collect(array_slice($grafik->toArray(), 0, $now))->sortKeys()->toArray();

        return array_merge($array2, $array1);
    }
}

function formatArrayTostring($array)
{
    return explode(',', implode(',', $array));
}

function getMonthRange($bulanSekarang, $bulanSebelumnya)
{
    $array = [
        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    ];

    $now = intval($bulanSekarang);
    $before = intval($bulanSebelumnya);
    $beforeChange = $before == 12 ? 0 : $before;
    if ($before <= 7 || $beforeChange == 0) {
        return $slice = collect(array_slice($array, $beforeChange, 5))->sortKeys()->toArray();
    } else {
        $array1 = collect(array_slice($array, $beforeChange, 5))->sortKeys()->toArray();
        $array2 = collect(array_slice($array, 0, $now))->sortKeys()->toArray();

        return array_merge($array2, $array1);
    }
}

function getNoPembayaran()
{
    return 'TG-' . strtoupper(generateRandomString(5)) . '-' . date('ymd');
}

function getNoPengembalian()
{
    return 'PG-' . strtoupper(generateRandomString(5)) . '-' .  date('ymd');
}

function validationInstan($request, $rule)
{
    $validator = Validator::make($request, $rule);

    if ($validator->fails()) {
        return (object)[
            'status' => false,
            'data' => $validator->messages()
        ];
    } else {
        return (object)[
            'status' => true,
            'data' => []
        ];
    }
}

function storage_exist($path)
{
    return Storage::disk('public')->exists($path);
}

function storage_delete($path)
{
    return Storage::disk('public')->delete($path);
}

/**
 * Input File ke Storage
 *
 * @param string $public
 * @param object $image
 * @param string $name
 * @return void
 */

function storage_insert($public, $image, $name)
{
    return Storage::putFileAs($public, $image, $name);
}

function storage_insert2($file, $name)
{
    $file->move('/home/alluthfi/laravel/gudang-dev/storage/app/public/bukti/', $name);

    return $name;
}

/**
 * Input File Ke Storage di cek dulu ada tidaknya dan mengubah nama file
 *
 * @param object $file
 * @param string $oldFile
 * @param string $path
 * @return void
 */
function insertImage($file, $oldFile = '', $path)
{
    if ($file) {
        if (storage_exist($path . $oldFile)) {
            storage_delete($path . $oldFile);
        }
        $newImage = $file;
        $extension = $newImage->getClientOriginalExtension();
        $lastFile = md5(rand(5, 10) . time()) . '.' . $extension;
        $p = storage_insert2($file, $lastFile);
    } else {
        if ($oldFile != null) {
            $lastFile = $oldFile;
        } else {
            $lastFile = '';
        }
    }

    return $lastFile;
}

/**
 * Hapus Gambar dari Storage Check dulu file nya ada atau tidak
 *
 * @param string $file
 * @return void
 */
function deleteImage($file)
{
    if (storage_exist($file)) {
        storage_delete($file);
    }
}

function errorValidation($message)
{
    return setResponse(false, $message, 422, 'Validasi Gagal');
}
