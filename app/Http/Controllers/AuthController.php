<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if (Auth::check()) {
            $sales = table('model_has_roles')->where('model_id', getUser()->id)->first();
            if ($sales->role_id == 4) {
                return response()->json([
                    'token' => $token, 'user' => getUser(), 'status' => 'sales'
                ]);
            } else if ($sales->role_id == 2) {
                return response()->json([
                    'token' => $token, 'user' => getUser(), 'status' => 'keuangan'
                ]);
            } else {
                return response()->json(['error' => 'you are not a sales'], 400);
            }
        }
    }

    public function me()
    {
        $sales = table('model_has_roles')->where('model_id', getUser()->id)->first();
        if ($sales->role_id == 4) {
            return response()->json([
                'user' => getUser(), 'status' => 'sales'
            ]);
        } else {
            return response()->json([
                'user' => getUser(), 'status' => 'keuangan'
            ]);
        }
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());

        return response()->json([
            'status' => true,
            'message' => 'Logout success'
        ]);
    }
}
