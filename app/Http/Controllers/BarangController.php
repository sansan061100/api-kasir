<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\BarangRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{

    protected $barangRepo;

    public function __construct(BarangRepositoryInterface $barangRepo)
    {
        $this->barangRepo = $barangRepo;
    }

    public function index()
    {
        $barang = $this->barangRepo->all(request()->q, request()->kategori, request()->showAll);

        return setResponse(true, $barang);
    }

    public function findByKode($kode)
    {
        $barang = $this->barangRepo->findByKode($kode);
        if ($barang->id == null) {
            return setResponse(false, [], 422,  'Tidak ditemukan');
        }
        return setResponse(true, $barang);
    }

    public function checkStokByKode(Request $request)
    {
        $stok = $this->barangRepo->checkStokByKode($request->kode);
        if ($stok == '') {
            return setResponse(false, [], 422, 'Tidak ditemukan');
        } else {
            if ($request->jumlah > $stok) {
                return setResponse(false, $stok, 200, 'Stok hanya tersedia ' . $stok . ' pcs');
            }

            return setResponse(true, $stok, 200, 'Stok tersedia');
        }
    }

    public function storeHargaSales(Request $request)
    {
        DB::beginTransaction();

        try {

            /**
             * Delete All Harga Sales By Kategori & barangs_id
             */
            $this->barangRepo->deleteHargaSales($request->kategori);

            /**
             * Store Harga Sales
             */
            $store = $this->barangRepo->storeHargaSales($request->harga_sales, $request->kategori);

            DB::commit();

            return setResponse(true, $store, 200, 'Berhasil Atur Harga Sales');
        } catch (\Throwable $th) {
            db::rollBack();
            return $th;
            return setResponse(false, [], 200, 'Gagal Atur Harga Sales');
        }
    }
}
