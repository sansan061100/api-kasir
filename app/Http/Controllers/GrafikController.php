<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\GrafikRepositoryInterface;
use App\Repository\Interfaces\PesananRepositoryInterface;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;

class GrafikController extends Controller
{
    protected $grafikRepo, $pesananRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GrafikRepositoryInterface $grafikRepo, PesananRepositoryInterface $pesananRepo)
    {
        $this->grafikRepo = $grafikRepo;
        $this->pesananRepo = $pesananRepo;
    }

    public function barangTerlaris(Request $request)
    {
        $barangTerlaris = $this->grafikRepo->barangTerlaris($request->awal, $request->akhir, $request->kategori);

        $jumlahBarang = $barangTerlaris->reduce(function ($carry, $item) {
            return $carry + $item->jumlah;
        });

        return setResponse(true, [
            'data' => $barangTerlaris,
            'total' => $jumlahBarang
        ]);
    }

    public function transaksiTerbesar(Request $request)
    {

        $totalTransaksi = $this->pesananRepo->totalTransaksi($request->awal, $request->akhir, $request->metode);

        $transaksiTerbesar = $this->grafikRepo->transaksiTerbesar($request->awal, $request->akhir, $request->metode);

        return setResponse(true, [
            'total_transaksi' => $totalTransaksi->total,
            'transaksi_terbesar' => $transaksiTerbesar
        ]);
    }

    public function transaksiPerPeriode(Request $request)
    {
        $transaksiPerPeriode = $this->grafikRepo->transaksiPerPeriode($request->type);

        return setResponse(true, $transaksiPerPeriode);
    }
}
