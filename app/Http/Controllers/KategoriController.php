<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\KategoriRepositoryInterface;

class KategoriController extends Controller
{

    protected $kategoriRepo;

    public function __construct(KategoriRepositoryInterface $kategoriRepo)
    {
        $this->kategoriRepo = $kategoriRepo;
    }

    public function index()
    {
        $kategori = $this->kategoriRepo->all();

        return setResponse(true, $kategori);
    }
}
