<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\Transaksi;
use App\Repository\Interfaces\PelangganRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PelangganController extends Controller
{
    protected $pelangganRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PelangganRepositoryInterface $pelangganRepo)
    {
        $this->pelangganRepo = $pelangganRepo;
    }

    public function index(Request $request)
    {
        $pelanggan = $this->pelangganRepo->all($request->q, $request->showAll);

        return setResponse(true, $pelanggan);
    }

    public function findById($id)
    {
        $pelanggan = $this->pelangganRepo->findById($id);

        return setResponse(true, $pelanggan);
    }

    public function cekHutang($id = null)
    {
        $pelanggan = $this->pelangganRepo->cekHutang($id);
        return setResponse(true, $pelanggan);
    }

    public function cekHutangAll()
    {
        $sales = table('model_has_roles')->where('model_id', getUser()->id)->first();

        $hutang = Transaksi::select(DB::raw('SUM(IF(transaksis.status = 3,jumlah,0)-IF(transaksis.status = 5,jumlah,0)-IF(transaksis.status = 0,jumlah,0)) as jumlah'))
            ->where('transaksis.status_pembayaran', 1)
            ->leftJoin('users', 'users.id', '=', 'transaksis.user_id')
            ->leftJoin('admins', 'admins.id', '=', 'users.admin_id')
            ->where(function ($query) use ($sales) {
                $sales->role_id == 4 ? $query->where('admins.id', getUser()->id) : "";
            })
            ->first();
        return setResponse(true, $hutang);
    }

    public function store(Request $request)
    {
        $rule = [
            'nama' => 'required|string',
            'username' => 'string|min:8|alpha_dash|unique:users,username',
            'no_hp' => 'min:11|max:13,unique:users,no_hp',
            'alamat' => 'string',
            'foto_profil' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            'foto_ktp' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ];

        $validation = validationInstan($request->all(), $rule);
        $code = generateRandomString(15);
        if ($validation->status == true) {
            $foto_ktp = insertImage($request->file('foto_ktp'), '', 'pelanggan');
            $foto_profil = insertImage($request->file('foto_profil'), '', 'pelanggan');

            $store = $this->pelangganRepo->store([
                'name' => $request->nama,
                'username' => $request->username,
                'password' => app('hash')->make($request->username),
                'no_hp' => $request->no_hp,
                'alamat' => $request->alamat,
                'level' => $request->level,
                'admin_id' => getUser()->id,
                'avatar' => $foto_profil,
                'foto_ktp' => $foto_ktp,
                'status_pelanggan' => 'pelanggan',
                'tagihan_awal' => $request->tagihan_awal,
                'tagihan_code' => $code
            ], $request->tagihan_awal);

            return setResponse(true, $store, 200, 'Berhasil Menambahkan Pelanggan');
        } else {
            return errorValidation($validation->data);
        }
    }
}
