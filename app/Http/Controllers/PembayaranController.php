<?php

namespace App\Http\Controllers;

use App\Repository\Interfaces\PelangganRepositoryInterface;
use App\Repository\Interfaces\PembayaranRepositoryInterface;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    protected $pembayaranRepo, $pelangganRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PembayaranRepositoryInterface $pembayaranRepo, PelangganRepositoryInterface $pelangganRepo)
    {
        $this->pembayaranRepo = $pembayaranRepo;
        $this->pelangganRepo = $pelangganRepo;
    }

    public function store(Request $request)
    {
        $rule = [
            'pelanggan' => 'required',
            'metode_pembayaran' => 'required',
            'jumlah_bayar' => 'required|numeric',
        ];

        $validation = validationInstan($request->all(), $rule);

        if ($validation->status == true) {

            $cekHutang = $this->pelangganRepo->findById($request->pelanggan);

            if ($cekHutang->piutang == 0) {
                return setResponse(false, $cekHutang, 200, 'Hutang Sudah Lunas');
            }

            $bukti = insertImage($request->file('bukti'), '', 'bukti');

            $pembayaran = $this->pembayaranRepo->store([
                'user_id' => $request->pelanggan,
                'no_pembayaran' => getNoPembayaran(),
                'jumlah' => $request->jumlah_bayar,
                'status' => 0,
                'no_rekening' => $request->metode_pembayaran,
                'status_pembayaran' => 0,
                'bukti' => $bukti,
                'keterangan' => 'Pembayaran Hutang',
                'tanggal' => date('Y-m-d H:i:s')
            ]);

            return setResponse(true, $pembayaran, 200, 'Berhasil Melakukan Pembayaran');
        } else {
            return errorValidation($validation->data);
        }
    }

    public function index(Request $request)
    {
        $pembayaran = $this->pembayaranRepo->all(getUser()->id, $request->pelanggan, $request->awal, $request->akhir);

        return setResponse(true, $pembayaran);
    }
}
