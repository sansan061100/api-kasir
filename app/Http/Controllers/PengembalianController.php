<?php

namespace App\Http\Controllers;

use App\Models\DetailPengembalian;
use App\Models\StokSales;
use App\Repository\Interfaces\BarangRepositoryInterface;
use App\Repository\Interfaces\PengembalianRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengembalianController extends Controller
{
    protected $pengembalianRepo, $barangRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PengembalianRepositoryInterface $pengembalianRepo, BarangRepositoryInterface $barangRepo)
    {
        $this->pengembalianRepo = $pengembalianRepo;
        $this->barangRepo = $barangRepo;
    }

    public function index(Request $request)
    {
        $pengembalian = $this->pengembalianRepo->all($request);

        return setResponse(true, $pengembalian);
    }

    public function store(Request $request)
    {
        $rule = [
            'pelanggan' => 'required'
        ];

        $validation = validationInstan($request->all(), $rule);

        if ($validation->status == true) {
            DB::beginTransaction();
            try {
                $total = 0;
                foreach ($request->barang as $key => $value) {
                    $total += $value['qty'] * $value['harga'];
                    $barang = $this->barangRepo->findByKode($value);
                    $detail[] = new DetailPengembalian([
                        'jumlah' => $value['qty'],
                        'harga' => $value['harga'],
                        'barang_sales_id' => $barang->id,
                    ]);

                    // Kembali Stok
                    StokSales::create([
                        'barang_sales_id' => $barang->id,
                        'jumlah' => $value['qty'],
                        'tanggal' => date('Y-m-d h:i:s'),
                        'status' => 2
                    ]);
                }

                $pengembalian = $this->pengembalianRepo->store([
                    'user_id' => $request->pelanggan,
                    'admin_id' => getUser()->id,
                    'no_pengembalian' => getNoPengembalian(),
                    'tanggal' =>  date('Y-m-d H:i:s'),
                    'status' => 1,
                    'alasan' => $request->alasan,
                    'type' => 1,
                    'total_pengembalian' => $total
                ], $detail);

                DB::commit();

                return setResponse(true, $pengembalian, 200, 'Berhasil Melakukan Pengembalian');
            } catch (\Throwable $th) {
                DB::rollback();
                return setResponse(false, [], 200, 'Gagal Melakukan Pengembalian');
                throw $th;
            }
        } else {
            return errorValidation($validation->data);
        }
    }

    public function findByKode($kode)
    {
        $pengembalian = $this->pengembalianRepo->findByKode($kode);

        return setResponse(true, $pengembalian);
    }
}
