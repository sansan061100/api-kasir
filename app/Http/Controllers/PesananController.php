<?php

namespace App\Http\Controllers;

use App\Models\Pesanan;
use App\Repository\Interfaces\PesananRepositoryInterface;
use Illuminate\Http\Request;

class PesananController extends Controller
{
    protected $pesananRepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PesananRepositoryInterface $pesananRepo)
    {
        $this->pesananRepo = $pesananRepo;
    }

    public function index(Request $request)
    {
        $pesanan = $this->pesananRepo->all($request->awal, $request->akhir, $request->pelanggan);

        return setResponse(true, $pesanan);
    }

    public function store(Request $request)
    {
        return $this->pesananRepo->store($request);
    }

    public function findByKode($kode)
    {
        $detailPesanan = $this->pesananRepo->findByKode($kode);

        return setResponse(true, $detailPesanan);
    }
}
