<?php

namespace App\Http\Controllers;

class RekeningController extends Controller
{
    public function index()
    {
        $rekening = table('banks')->selectRaw('CONCAT(tipe_bank," - ",no_rekening) as id, CONCAT(tipe_bank," - ",no_rekening) as nama')->get();

        return setResponse(true, $rekening);
    }
}
