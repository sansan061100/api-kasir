<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([
                    'code'   => 101, // means auth error in the api,
                    'response' => 'Authorization Token not found' // nothing to show
                ], 500);
            }
        } catch (TokenExpiredException $e) {
            // If the token is expired, then it will be refreshed and added to the headers
            try {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();

                return response()->json([
                    'token' => $refreshed
                ]);
            } catch (JWTException $e) {
                return response()->json([
                    'code'   => 103,
                    'response' => 'Token is Blacklist'
                ], 500);
            }
        } catch (JWTException $e) {
            return response()->json([
                'code'   => 101, // means auth error in the api,
                'response' => 'Token is Invalid' // nothing to show
            ], 500);
        }

        return  $next($request);
    }
}
