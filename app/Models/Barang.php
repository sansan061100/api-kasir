<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang_sales';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function hargaSales()
    {
        return $this->hasMany(HargaSales::class, 'barang_sales_id');
    }
}
