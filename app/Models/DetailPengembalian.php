<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPengembalian extends Model
{
    protected $table = 'detail__pengembalians';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function barang()
    {
        return $this->belongsTo(Barang::class, 'barang_sales_id');
    }
}
