<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HargaSales extends Model
{
    protected $table = 'harga_sales';

    protected $guarded = ['id'];

    protected $cast = [
        'harga_grosir' => 'integer',
        'minimum' => 'integer',
    ];

    public $timestamps = false;
}
