<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pesanan extends Model
{
    use SoftDeletes;
    protected $table = 'sub__orders';

    public $timestamps = false;

    protected $guarded = ['id'];

    public function detailPesanan()
    {
        return $this->hasMany(DetailPesanan::class, 'sub_order_id');
    }
}
