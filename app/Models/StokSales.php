<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StokSales extends Model
{
    protected $table = 'stok_sales';

    public $timestamps = false;

    protected $guarded = ['id'];
}
