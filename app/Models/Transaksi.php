<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaksi extends Model
{
    use SoftDeletes;
    protected $table = 'transaksis';

    public $timestamps = false;

    protected $guarded = ['id'];

    public static function insertTransaksi($data)
    {
        return Transaksi::create([
            'user_id' => $data['pelanggan'],
            'jumlah' => $data['total'],
            'status' => 0,
            'keterangan' => 'Dari Pengembalian Barang',
            'tanggal' => date('Y-m-d h:i:s'),
            'no_pembayaran' => 'TG-' . strtoupper(generateRandomString(5)) . '-' . date('ymd'),
            'status_pembayaran' => 1,
        ]);
    }

    public static function insertTransaksiTwo($data, $status_giro = true)
    {
        $transaksi = Transaksi::create([
            'user_id' => $data['pelanggan'],
            'sub_order_id' => $data['sub_order_id'],
            'no_pembayaran' => 'TG-' . strtoupper(generateRandomString(5)) . '-' . date('ymd'),
            'jumlah' => $data['jumlah'],
            'status' => 0,
            'status_pembayaran' => $status_giro,
            'no_rekening' => $data['no_rekening'],
            'keterangan' => 'Pembayaran',
            'tanggal' => date('Y-m-d H:i:s'),
        ]);
    }
}
