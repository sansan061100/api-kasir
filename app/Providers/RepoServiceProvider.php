<?php

namespace App\Providers;

use App\Repository\BarangRepository;
use App\Repository\GrafikRepository;
use App\Repository\Interfaces\BarangRepositoryInterface;
use App\Repository\Interfaces\GrafikRepositoryInterface;
use App\Repository\Interfaces\KategoriRepositoryInterface;
use App\Repository\Interfaces\PelangganRepositoryInterface;
use App\Repository\Interfaces\PembayaranRepositoryInterface;
use App\Repository\Interfaces\PengembalianRepositoryInterface;
use App\Repository\Interfaces\PesananRepositoryInterface;
use App\Repository\KategoriRepository;
use App\Repository\PelangganRepository;
use App\Repository\PembayaranRepository;
use App\Repository\PengembalianRepository;
use App\Repository\PesananRepository;
use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(KategoriRepositoryInterface::class, KategoriRepository::class);
        $this->app->bind(BarangRepositoryInterface::class, BarangRepository::class);
        $this->app->bind(PelangganRepositoryInterface::class, PelangganRepository::class);
        $this->app->bind(PesananRepositoryInterface::class, PesananRepository::class);
        $this->app->bind(GrafikRepositoryInterface::class, GrafikRepository::class);
        $this->app->bind(PembayaranRepositoryInterface::class, PembayaranRepository::class);
        $this->app->bind(PengembalianRepositoryInterface::class, PengembalianRepository::class);
    }
}
