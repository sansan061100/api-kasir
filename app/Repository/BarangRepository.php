<?php

namespace App\Repository;

use App\Models\Barang;
use App\Models\HargaSales;
use App\Repository\Interfaces\BarangRepositoryInterface;

class BarangRepository implements BarangRepositoryInterface
{
    public function all($search = null, $kategori = null, $showAll = false)
    {
        $barang = Barang::where(function ($query) use ($search) {
            $query->where('kode_barang', 'like', '%' . $search . '%')
                ->orWhere('nama_barang', 'like', '%' . $search . '%');
        })
            ->leftJoin('stok_sales', 'stok_sales.barang_sales_id', 'barang_sales.id')
            ->where('barang_sales.admins_id', getUser()->id)
            ->when($kategori, function ($query) use ($kategori) {
                $query->where('kategori', $kategori);
            })
            ->selectRaw('barang_sales.id,kode_barang as kode, nama_barang as nama,SUM(IF(stok_sales.status = 0,jumlah,0) + IF(stok_sales.status = 5,jumlah,0) + IF(stok_sales.status = 2,jumlah,0) - IF(stok_sales.status = 1,jumlah,0)) as stok')
            ->with([
                'hargaSales' => function ($query) {
                    $query->orderBy('minimum', 'ASC');
                }
            ])
            ->where(function ($query) use ($showAll) {
                if ($showAll == false || $showAll == null) {
                    $query->whereHas('hargaSales');
                }
            })
            ->addSelect([
                'gambar' => table('gambar_barang_sales')->whereColumn('gambar_barang_sales.barang_sales_id', 'barang_sales.id')
                    ->where('gambar_barang_sales.is_thumbnail', 0)
                    ->selectRaw('IFNULL(gambar_barang_sales.nama_gambar, gambar__barangs.nama_gambar) as gambar')
                    ->leftJoin('gambar__barangs', 'gambar__barangs.id', 'gambar_barang_sales.gambar__barangs_id')
            ])
            ->groupBy('barang_sales.id');

        if ($showAll == false || $showAll == null) {
            $barang->having('stok', '>', 0);
        }

        return $barang->paginate(10);
    }

    public function findByKode($kode)
    {
        return Barang::leftJoin('stok_sales', 'stok_sales.barang_sales_id', 'barang_sales.id')
            ->selectRaw('barang_sales.id, nama_barang as nama, kode_barang as kode, harga, SUM(IF(stok_sales.status = 0,jumlah,0) + IF(stok_sales.status = 5,jumlah,0) + IF(stok_sales.status = 2,jumlah,0) - IF(stok_sales.status = 1,jumlah,0)) as jumlah, harga_sales as harga_grosir')
            ->where(function ($query) use ($kode) {
                $query->where('barang_sales.id', $kode)->orWhere('barang_sales.kode_barang', $kode);
            })->where('barang_sales.admins_id', getUser()->id)
            ->first();
    }

    public function checkStokByKode($kode)
    {
        $find = $this->findByKode($kode);

        return $find->jumlah;
    }

    public function barangByKategori($kategori)
    {
        return Barang::where([
            'admins_id' => getUser()->id,
            'kategori' => $kategori
        ])->get();
    }

    public function deleteHargaSales($kategori)
    {
        $barangByKategori = $this->barangByKategori($kategori);

        foreach ($barangByKategori as $key => $value) {
            HargaSales::where('barang_sales_id', $value->id)->delete();
        }

        return true;
    }

    public function storeHargaSales($harga_sales, $kategori)
    {
        $barangByKategori = $this->barangByKategori($kategori);
        foreach ($barangByKategori as $value) {
            foreach ($harga_sales as $value2) {
                $harga_grosir = [
                    'minimum' => $value2['minimum'],
                    'harga_grosir' => $value2['harga_grosir'],
                    'barang_sales_id' => $value['id']
                ];
                HargaSales::insert($harga_grosir);
            }
        }
        return true;
    }
}
