<?php

namespace App\Repository;

use App\Models\Pesanan;
use App\Repository\Interfaces\GrafikRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class GrafikRepository implements GrafikRepositoryInterface
{
    public function barangTerlaris($awal = null, $akhir = null, $kategori = null)
    {
        return DB::table('detail__orders')
            ->join('barang_sales', 'detail__orders.barang_sales_id', '=', 'barang_sales.id')
            ->join('sub__orders', 'sub__orders.id', '=', 'detail__orders.sub_order_id')
            ->where('sub__orders.admin_id', getUser()->id)
            ->whereIn('sub__orders.status', [4, 5])
            ->when($awal != null && $akhir != null, function ($query) use ($awal, $akhir) {
                $query->whereBetween('sub__orders.tanggal', [dateInputFormat($awal, 'awal'), dateInputFormat($akhir, 'akhir')]);
            })
            ->when($kategori, function ($query) use ($kategori) {
                $query->where('kategori', $kategori);
            })
            ->groupBy('barang_sales.id')
            // ->limit(5)
            ->orderBy('jumlah', 'desc')
            ->select('barang_sales.kode_barang', 'barang_sales.nama_barang', DB::raw('SUM(detail__orders.jumlah) as jumlah'))
            ->get();
    }

    public function transaksiTerbesar($awal = null, $akhir = null, $metode = null)
    {
        $transaksi =  DB::table('sub__orders')
            ->leftJoin('users', 'sub__orders.user_id', 'users.id')
            ->leftJoin('admins', 'admins.id', 'users.admin_id')
            ->whereNull('deleted_at')
            ->whereIn('sub__orders.status', [4, 5])
            ->groupBy('sub__orders.user_id')
            ->where('users.admin_id', getUser()->id)
            ->where('sub__orders.jenis_pesanan', 'tamu')
            ->when($awal != null && $akhir != null, function ($query) use ($awal, $akhir) {
                $query->whereBetween('sub__orders.tanggal', [dateInputFormat($awal, 'awal'), dateInputFormat($akhir, 'akhir')]);
            })
            // ->orderBy('total', 'desc')
            // ->limit(10)
            ->select('users.name as nama', 'users.id');

        if ($metode != null) {
            if ($metode == CASH) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_cash - kembalian) as total'), DB::raw('COUNT(jumlah_cash) as jumlah'))->whereIn('status_pembayaran', [CASH, CASH_GIRO, CASH_TRANSFER, CASH_KREDIT]);
            } elseif ($metode == TRANSFER) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_tf - kembalian) as total'), DB::raw('COUNT(jumlah_tf) as jumlah'))->whereIn('status_pembayaran', [TRANSFER, CASH_TRANSFER, TRANSFER_GIRO, TRANSFER_KREDIT]);
            } elseif ($metode == GIRO) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_giro) as total'), DB::raw('COUNT(jumlah_giro) as jumlah'))->whereIn('status_pembayaran', [GIRO, CASH_GIRO, TRANSFER_GIRO]);
            } elseif ($metode == KREDIT) {
                $transaksi->addSelect(DB::raw('SUM(total_pesanan - dibayar) as total'), DB::raw('COUNT(*) as jumlah'))->whereIn('status_pembayaran', [KREDIT, CASH_KREDIT, TRANSFER_KREDIT]);
            }
        } else {
            $transaksi->addSelect(DB::raw('SUM(total_pesanan) as total'), DB::raw('COUNT(*) as jumlah'));
        }
        return $transaksi->having('total', '>', 0)->orderBy('total', 'DESC')->having('jumlah', '>', 0)->get();
    }

    public function transaksiPerPeriode($type)
    {
        $transaksi = Pesanan::where('admin_id', getUser()->id);

        $formatGrafik = [];

        if ($type == HARI) {
            $perDay = $transaksi->whereBetween('tanggal', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                ->selectRaw('DAYOFWEEK(tanggal) as week, dibayar as total')
                ->groupBy('week')
                ->get();

            $grafikByDay = grafikByWeek($perDay);
            $formatGrafik['label'] = getAllWeekName();
            $formatGrafik['totalTransaksi'] = formatGrafik($grafikByDay);
        } elseif ($type == MINGGU) {
            $perWeek = $transaksi->whereBetween('tanggal', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])
                ->selectRaw("ceiling((day(tanggal) -
            (6 - weekday(date_format(tanggal,'%Y-%m-01'))))/7)
            + case when 6 - weekday(date_format(tanggal,'%Y-%m-01'))> 0
            then 1 else 0 end week, SUM(dibayar) as total")
                ->groupBy(DB::raw('WEEK(tanggal)'))
                ->get();

            $grafikByWeek = grafikByMonthGroupWeek($perWeek);
            $formatGrafik['label'] = getAllQuarterWeekName();
            $formatGrafik['totalTransaksi'] = formatGrafik($grafikByWeek);
        } else {
            $bulanSekarang = Carbon::now()->endOfMonth();
            $bulanSebelumnya = Carbon::now()->subMonth(5)->startOfMonth();

            $perMonth = $transaksi->whereBetween('tanggal', [$bulanSebelumnya, $bulanSekarang])
                ->selectRaw('MONTH(tanggal) as month, SUM(dibayar) as total')
                ->orderBy('month')
                ->groupBy('month')
                ->get();

            $grafikByMonth = grafikByMonthApi($perMonth, $bulanSekarang->format('m'), $bulanSebelumnya->format('m'));
            $formatGrafik['label'] = formatArrayTostring(getMonthRange($bulanSekarang->format('m'), $bulanSebelumnya->format('m')));
            $formatGrafik['totalTransaksi'] = formatGrafik($grafikByMonth);
        }

        return $formatGrafik;
    }
}
