<?php

namespace App\Repository\Interfaces;

interface BarangRepositoryInterface
{
    public function all($search = null, $kategori = null, $showAll = false);

    public function findByKode($kode);

    public function checkStokByKode($kode);

    public function deleteHargaSales($kategori);

    public function storeHargaSales($harga_sales, $kategori);
}
