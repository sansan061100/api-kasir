<?php

namespace App\Repository\Interfaces;

interface GrafikRepositoryInterface
{
    public function barangTerlaris($awal = null, $akhir = null, $kategori = null);

    public function transaksiTerbesar($awal = null, $akhir = null, $metode = null);

    public function transaksiPerPeriode($type);
}
