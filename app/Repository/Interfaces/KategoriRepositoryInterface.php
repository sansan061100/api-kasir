<?php

namespace App\Repository\Interfaces;

interface KategoriRepositoryInterface
{
    public function all();
}
