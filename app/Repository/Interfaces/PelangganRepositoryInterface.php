<?php

namespace App\Repository\Interfaces;

interface PelangganRepositoryInterface
{
    public function all($search = null, $showAll = false);

    public function cekHutang($id);

    public function store($data, $tagihan_awal);

    public function findById($id);
}
