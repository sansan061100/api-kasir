<?php

namespace App\Repository\Interfaces;

interface PembayaranRepositoryInterface
{
    public function store($data);

    public function all($sales = null, $user = null, $awal = null, $akhir = null);
}
