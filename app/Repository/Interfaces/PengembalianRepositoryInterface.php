<?php

namespace App\Repository\Interfaces;

interface PengembalianRepositoryInterface
{
    public function all($request);

    public function store($pengembalian, $detail);

    public function findByKode($kode);
}
