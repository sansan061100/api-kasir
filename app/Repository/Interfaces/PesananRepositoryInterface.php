<?php

namespace App\Repository\Interfaces;

interface PesananRepositoryInterface
{
    public function all($awal = null, $akhir = null, $pelanggan = null);

    public function store($request);

    public function findByKode($kode);

    public function totalTransaksi($awal = null, $akhir = null, $metode = null);
}
