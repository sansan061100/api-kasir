<?php

namespace App\Repository;

use App\Repository\Interfaces\KategoriRepositoryInterface;

class KategoriRepository implements KategoriRepositoryInterface
{
    public function all()
    {
        return table('kategoris')->get();
    }
}
