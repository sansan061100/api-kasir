<?php

namespace App\Repository;

use App\Models\Pelanggan;
use App\Models\Transaksi;
use App\Repository\Interfaces\PelangganRepositoryInterface;
use Illuminate\Support\Facades\DB;

class PelangganRepository implements PelangganRepositoryInterface
{
    public function all($search = null, $showAll = false)
    {
        return Pelanggan::selectRaw('users.id, users.name as nama,admins.name as sales,users.alamat, SUM(IF(transaksis.status = 3,jumlah,0)-IF(transaksis.status = 0 AND transaksis.status_pembayaran = 1,jumlah,0)) as jumlah')
            ->leftJoin('transaksis', 'transaksis.user_id', 'users.id')->leftJoin('admins', 'admins.id', 'users.admin_id')
            ->where(function ($query) use ($search, $showAll) {
                $query->where('users.name', 'LIKE', '%' . $search . '%');
                $sales = table('model_has_roles')->where('model_id', getUser()->id)->first();

                if ($sales->role_id == 4) {
                    $query->where('users.admin_id', getUser()->id);
                }
            })
            ->where('status_pelanggan', 'pelanggan')
            ->groupBy('users.id')
            ->paginate(20);
    }

    public function cekHutang($id)
    {

        // $sisaTagihan = 0;

        $tahunLalu = Transaksi::leftJoin('sub__orders', 'sub__orders.id', 'transaksis.sub_order_id')
            ->select(DB::raw('IF(transaksis.status = 3,sub__orders.no_order,transaksis.no_pembayaran) as no'), 'transaksis.tanggal', DB::raw('SUM(IF(transaksis.status = 3,jumlah,0)-IF(transaksis.status = 5,jumlah,0)-IF(transaksis.status = 0,jumlah,0)) as jumlah'))
            ->where('transaksis.status_pembayaran', 1)
            ->where('transaksis.user_id', $id)
            ->whereYear('transaksis.tanggal', '!=', date('Y'))
            ->get();

        $transaksi = Transaksi::leftJoin('sub__orders', 'sub__orders.id', 'transaksis.sub_order_id')
            ->selectRaw('IF(transaksis.status = 3,transaksis.jumlah,0) as tagihan, IF(transaksis.status = 0,transaksis.jumlah,0) as dibayar, IF(transaksis.status = 5,transaksis.jumlah,0) as diskon,
            CASE WHEN transaksis.keterangan = "Piutang" THEN "Tagihan"
            WHEN transaksis.keterangan = "Pembayaran Hutang" THEN "Bayar"
            WHEN transaksis.keterangan = "Dari Pengembalian Barang" THEN "Pengembalian Barang"
            WHEN transaksis.keterangan = "Pembayaran" THEN "Bayar"
            ELSE "Diskon Tagihan" END AS keterangan
            , transaksis.tanggal,transaksis.status')
            ->whereIn('transaksis.status', [0, 3, 5])
            ->orderBy(DB::raw("DATE_FORMAT(transaksis.tanggal,'%d-%M-%Y')"), 'asc')
            ->orderBy('transaksis.status', 'desc')
            ->where('transaksis.status_pembayaran', 1)
            ->where('transaksis.user_id', $id)
            ->whereYear('transaksis.tanggal', date('Y'))
            ->get();

        $changeTahunLalu =  collect($tahunLalu)->map(function ($tahunLalu) {

            $tahunLalu->tanggal = "1111-11-11 02:02:47";
            $tahunLalu->tagihan = $tahunLalu->jumlah ?? 0;
            $tahunLalu->dibayar = 0;
            $tahunLalu->keterangan = "Tahun Lalu";

            return $tahunLalu;
        })->merge($transaksi)->values();

        return collect($changeTahunLalu)->sortBy('tanggal')->map(function ($changeTahunLalu) use (&$sisaTagihan) {
            $sisaTagihan += $changeTahunLalu->tagihan - $changeTahunLalu->dibayar - $changeTahunLalu->diskon;

            $changeTahunLalu->sisa_tagihan = $sisaTagihan;

            if ($changeTahunLalu->tagihan == 0) {
                $changeTahunLalu->tagihan = $changeTahunLalu->diskon ?? 0;
            } else {
                $changeTahunLalu->tagihan = $changeTahunLalu->tagihan;
            }
            return $changeTahunLalu;
        })->values();
    }

    public function store($data, $tagihan_awal)
    {

        $store = Pelanggan::create($data);

        if ($tagihan_awal > 0) {
            $tagihan_awal = Transaksi::create([
                'user_id' => $store->id,
                'jumlah' => $tagihan_awal,
                'status' => 3,
                'status_pembayaran' => 1,
                'keterangan' => 'Piutang',
                'tanggal' => date('Y-m-d H:i:s'),
                'tagihan_code' => $store->tagihan_code
            ]);
        }

        return $store;
    }

    public function findById($id)
    {
        $saldo = Transaksi::where('user_id', $id)
            ->selectRaw('SUM(IF(status = 2,jumlah,0)-IF(status = 1,jumlah,0)+IF(status = 4,jumlah,0)) as jumlah')
            ->first();

        $piutang = Transaksi::where('user_id', $id)
            ->selectRaw('SUM(IF(status = 3,jumlah,0)-IF(status = 5, jumlah,0)-IF(status = 0,jumlah,0)) as jumlah')
            ->where('status_pembayaran', 1)
            ->first();
        $pelanggan =  Pelanggan::select('id', 'name as nama', 'no_hp', 'alamat', 'avatar')
            ->where([
                'id' => $id,
                'admin_id' => getUser()->id
            ])
            ->first();

        $pelanggan->piutang = $piutang->jumlah < 0 ? 0 : $piutang->jumlah;
        $pelanggan->saldo = $saldo->jumlah ?? 0;

        return $pelanggan;
    }
}
