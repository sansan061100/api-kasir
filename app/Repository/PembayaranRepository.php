<?php

namespace App\Repository;

use App\Models\Transaksi;
use App\Repository\Interfaces\PembayaranRepositoryInterface;
use Illuminate\Support\Facades\DB;

class PembayaranRepository implements PembayaranRepositoryInterface
{
    public function store($data)
    {
        return Transaksi::create($data);
    }

    public function all($sales = null, $user = null, $awal = null, $akhir = null)
    {
        return Transaksi::leftJoin('users', 'users.id', 'transaksis.user_id')
            ->where('transaksis.status', 0)
            ->whereNull('deleted_at')
            ->orderBy('tanggal', 'desc')
            ->select('transaksis.id', 'users.name as nama', 'transaksis.tanggal', 'jumlah', 'status_pembayaran', 'no_pembayaran', 'keterangan', DB::raw('IF(no_rekening = "Cash", "Tunai",no_rekening) as metode_pembayaran'))
            ->when($sales, function ($query) use ($sales) {
                $query->where('admin_id', $sales);
            })
            ->when($user, function ($query) use ($user) {
                $query->where('transaksis.user_id', $user);
            })
            ->when($awal != null && $akhir != null, function ($query) use ($awal, $akhir) {
                $query->whereBetween('tanggal', [
                    dateInputFormat($awal, 'awal'),
                    dateInputFormat($akhir, 'akhir'),
                ]);
            })
            ->groupBy('transaksis.id')
            ->paginate(10);
    }
}
