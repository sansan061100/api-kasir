<?php

namespace App\Repository;

use App\Models\Pengembalian;
use App\Models\Transaksi;
use App\Repository\Interfaces\PengembalianRepositoryInterface;

class PengembalianRepository implements PengembalianRepositoryInterface
{
    public function all($request)
    {
        return Pengembalian::select('pengembalians.id', 'no_pengembalian', 'total_pengembalian as jumlah', 'name as nama', 'tanggal')
            ->leftJoin('users', 'users.id', 'pengembalians.user_id')
            ->where([
                'pengembalians.admin_id' => getUser()->id,
                'type' => 1
            ])
            ->when($request->awal != null && $request->akhir != null, function ($query) use ($request) {
                $query->whereBetween('tanggal', [
                    dateInputFormat($request->awal, 'awal'),
                    dateInputFormat($request->akhir, 'akhir'),
                ]);
            })
            ->orderBy('pengembalians.tanggal', 'DESC')
            ->paginate(10);
    }

    public function store($pengembalian, $detail)
    {
        // Cashback
        $cashback = Transaksi::insertTransaksi([
            'pelanggan' => $pengembalian['user_id'],
            'total' => $pengembalian['total_pengembalian']
        ]);

        // Pengembalian
        $pengembalian = Pengembalian::create($pengembalian);

        // Detail Pengembalian
        return $pengembalian->detailPengembalian()->saveMany($detail);
    }

    public function findByKode($kode)
    {
        return Pengembalian::where([
            'no_pengembalian' => $kode,
            'type' => 1
        ])
            ->leftJoin('users', 'users.id', 'pengembalians.user_id')
            ->with(
                [
                    'detailPengembalian' => function ($query) {
                        $query->with([
                            'barang' => function ($query) {
                                $query->select('id', 'kode_barang', 'nama_barang');
                            }
                        ])->select('id', 'barang_sales_id', 'pengembalian_id', 'jumlah', 'harga');
                    }
                ]
            )
            ->selectRaw('pengembalians.*,users.name as nama')
            ->first();
    }
}
