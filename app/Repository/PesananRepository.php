<?php

namespace App\Repository;

use App\Models\Barang;
use App\Models\DetailPesanan;
use App\Models\Pelanggan;
use App\Models\Pesanan;
use App\Models\StokSales;
use App\Models\Transaksi;
use App\Repository\Interfaces\PesananRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PesananRepository implements PesananRepositoryInterface
{
    public function all($awal = null, $akhir = null, $pelanggan = null)
    {
        return $data = Pesanan::orderBy('tanggal', 'desc')
            ->where('sub__orders.status', 5)
            ->whereNull('deleted_at')
            ->where('jenis_pesanan', 'tamu')
            ->leftJoin('users', 'users.id', 'sub__orders.user_id')
            ->select('no_order as kode', 'tanggal', 'total_pesanan as total', 'name as nama', 'status_hutang', 'status_pembayaran', 'jumlah_tf', 'jumlah_cash')
            ->when($awal != null && $akhir != null, function ($query) use ($awal, $akhir) {
                $query->whereBetween('sub__orders.tanggal', [
                    dateInputFormat($awal, 'awal'),
                    dateInputFormat($akhir, 'akhir'),
                ]);
            })
            ->when($pelanggan, function ($query) use ($pelanggan) {
                $query->where('sub__orders.user_id', $pelanggan);
            })
            ->where(function ($query) {
                $sales = table('model_has_roles')->where('model_id', getUser()->id)->first();
                if ($sales->role_id == 4) {
                    $query->where('sub__orders.admin_id', getUser()->id);
                }
            })
            ->groupBy('sub__orders.id')
            ->paginate(10);
    }

    public function store($request)
    {
        DB::beginTransaction();

        try {
            $jumlah_bayar = $request->jumlah_bayar;
            $cart = $request->cart;
            $total_pesanan = 0;

            /**
             * Search Tamu
             */
            $searchTamu = Pelanggan::where([
                'status_pelanggan' => 'tamu',
                'admin_id' => getUser()->id
            ])->first();

            /**
             * Cek Apakah Tamu atau Pelanggan
             */
            $pelanggan = $request->pelanggan == null ? $searchTamu->id : $request->pelanggan;

            /**
             * Proses Create Array Detail Pesanan
             */
            foreach ($cart as $item) {

                $jumlah_beli = ($item['kodi'] * 20) + $item['qty'];
                $searchBarang = Barang::where('kode_barang', $item['kode'])
                    ->select('harga', 'id')
                    ->with('hargaSales')
                    ->where('admins_id', getUser()->id)
                    ->first();

                $hargaSales = collect($searchBarang->hargaSales)
                    ->where('minimum', '<=', $jumlah_beli)
                    ->sortByDesc('minimum')
                    ->first();


                $harga = 0;
                if ($hargaSales == null) {
                    if (!isset($item['harga_baru'])) {
                        $harga = $searchBarang->harga;
                    } else {
                        $harga = $item['harga_baru'];
                    }
                } else {
                    if (!isset($item['harga_baru'])) {
                        $harga = $hargaSales->harga_grosir;
                    } else {
                        $harga = $item['harga_baru'];
                    }
                }

                $total_pesanan += ($harga - ($item['diskon'] ?? 0)) * $jumlah_beli;


                $detail = [
                    'barang_sales_id' => $searchBarang->id,
                    'jumlah' => $jumlah_beli,
                    'harga' => $harga ?? 0,
                    'diskon' => $item['diskon'] ?? 0
                ];

                $detail_pesanan[] = new DetailPesanan($detail);
            }

            // Total yg harus dibayarkan
            $total_harus_bayar = intval($total_pesanan) - intval($request->diskon);
            $total_bayar = intval($jumlah_bayar) > intval($total_harus_bayar) ? intval($total_harus_bayar) : intval($jumlah_bayar);

            // Kembalian
            if (intval($jumlah_bayar) > $total_harus_bayar) {
                $kembalian = $jumlah_bayar - $total_harus_bayar;
            } else {
                $kembalian = 0;
            }

            /**
             * Cek apakah Pelanggan atau Bukan &
             * Cek Apakah Jumlah Bayar kurang dari Total Harus Bayar
             * Jika Iya maka proses dihentikan
             */
            if ($request->pelanggan == null && $jumlah_bayar < ($total_harus_bayar)) {
                return setResponse(false, [], 200,  'Nominal yang dibayar kurang');
            }

            /**
             * Cek Apakah Jumlah Bayar kurang dari Total Harus Bayar
             * Jika Iya maka status Hutang
             * Jika Tidak maka status Lunas
             */
            if ($jumlah_bayar < $total_harus_bayar) {
                $statusHutang = HUTANG;
            } else {
                $statusHutang = LUNAS;
            }

            /**
             * Cek Jika Status Pembayaran Tunai Transfer / Tunai Giro
             */
            if ($request->status_pembayaran == CASH_TRANSFER) {
                if ($request->jumlah_cash == 0 || $request->jumlah_tf == 0) {
                    return setResponse(false, [], 200, 'Jumlah yang dibayarkan tidak boleh kosong !');
                }
            }

            if ($request->status_pembayaran == CASH_GIRO) {
                if ($request->jumlah_cash == 0 || $request->jumlah_giro == 0) {
                    return setResponse(false, [], 200, 'Jumlah yang dibayarkan tidak boleh kosong !');
                }
            }

            /**
             * Cek Jika Pembayaran Tunai / Transfer
             */
            if ($request->status_pembayaran == CASH || $request->status_pembayaran == TRANSFER) {
                if ($jumlah_bayar == 0) {
                    return setResponse(false, [], 200, 'Jumlah yang dibayarkan tidak boleh kosong !');
                }
            }

            /**
             * Cek Jumlah Bayar Jika tidak Lunas di Status
             * Pembayaran Cash & Transfer
             */
            if ($request->status_pembayaran == CASH && $jumlah_bayar < $total_harus_bayar) {
                $status_pembayaran = CASH_KREDIT;
            } elseif ($request->status_pembayaran == TRANSFER && $jumlah_bayar < $total_harus_bayar) {
                $status_pembayaran = TRANSFER_KREDIT;
            } else {
                $status_pembayaran = $request->status_pembayaran;
            }

            // Data Pesanan
            $pesanan = [
                'admin_id' => getUser()->id,
                'user_id' => $pelanggan,
                'no_order' => 'OD-' . strtoupper(generateRandomString(5)) . '-' . date('Ymd'),
                'status' => 5,
                'tanggal' => date('Y-m-d H:i:s'),
                'dibayar' => $jumlah_bayar ?? 0,
                'jumlah_cash' => $request->jumlah_cash,
                'jumlah_tf' => $request->jumlah_tf,
                'jumlah_giro' => $request->jumlah_giro,
                'diskon' => $request->diskon,
                'kembalian' => $kembalian,
                'total_pesanan' => intval($total_pesanan) - intval($request->diskon),
                'jenis_pesanan' => 'tamu',
                'status_hutang' => $statusHutang,
                'status_pembayaran' => $status_pembayaran
            ];

            // Store Pesanan
            $storePesanan = Pesanan::create($pesanan);

            if ($storePesanan) {

                // Save Detail Pesanan
                $detailPesananArray = $storePesanan->detailPesanan()->saveMany($detail_pesanan);

                // Pengurangan Stok
                foreach ($detailPesananArray as $key => $value) {
                    StokSales::create([
                        'barang_sales_id' => $value->barang_sales_id,
                        'detail_order_id' => $value->id,
                        'jumlah' => $value->jumlah,
                        'tanggal' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ]);
                }

                $no_rekening = [
                    'Cash', 'Transfer', 'BG/Giro'
                ];

                // Piutang
                $transaksi = Transaksi::create([
                    'user_id' => $pelanggan,
                    'sub_order_id' => $storePesanan->id,
                    'jumlah' => $total_harus_bayar,
                    'status' => 3,
                    'status_pembayaran' => 1,
                    'keterangan' => 'Piutang',
                    'tanggal' => date('Y-m-d H:i:s'),
                ]);

                // Transaksi
                if ($jumlah_bayar > 0) {
                    if ($request->status_pembayaran == CASH || $request->status_pembayaran == TRANSFER) {
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $total_bayar,
                            'no_rekening' => $no_rekening[$request->status_pembayaran],
                        ]);
                    } elseif ($request->status_pembayaran == CASH_TRANSFER) {

                        // Cash
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_cash,
                            'no_rekening' => $no_rekening[CASH]
                        ]);

                        // Transfer
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_tf,
                            'no_rekening' => $no_rekening[TRANSFER]
                        ]);
                    } elseif ($request->status_pembayaran == CASH_GIRO) {
                        // Cash
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_cash,
                            'no_rekening' => $no_rekening[CASH]
                        ]);

                        // Giro
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_giro,
                            'no_rekening' => $no_rekening[GIRO]
                        ], false);
                    } elseif ($request->status_pembayaran == GIRO) {
                        // Giro
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $total_bayar,
                            'no_rekening' => $no_rekening[GIRO]
                        ], false);
                    } elseif ($request->status_pembayaran == TRANSFER_GIRO) {
                        // Transfer
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_tf,
                            'no_rekening' => $no_rekening[TRANSFER]
                        ]);

                        // Giro
                        $transaksi = Transaksi::insertTransaksiTwo([
                            'pelanggan' => $pelanggan,
                            'sub_order_id' => $storePesanan->id,
                            'jumlah' => $request->jumlah_giro,
                            'no_rekening' => $no_rekening[GIRO]
                        ], false);
                    }
                }
            }
            DB::commit();
            return setResponse(true, $storePesanan, 200, 'Berhasil Menambahkan Pesanan');
        } catch (\Throwable $th) {
            return $th;
            DB::rollback();
            return setResponse(false, [], 200, 'Gagal Menambahkan Pesanan');
        }
    }

    public function findByKode($kode)
    {
        return $detail = Pesanan::where('no_order', $kode)
            ->with([
                'detailPesanan' => function ($query) {
                    $query->with([
                        'barang' => function ($query) {
                            $query->select('nama_barang', 'id');
                        }
                    ]);
                }
            ])
            ->selectRaw('no_order as kode, sub__orders.tanggal, total_pesanan as total, dibayar, sub__orders.id, users.name as pelanggan, diskon, admins.name as sales,
            (
                CASE WHEN status_pembayaran = ' . CASH . ' THEN "Tunai"
                WHEN status_pembayaran = ' . TRANSFER . ' THEN "Transfer"
                WHEN status_pembayaran = ' . CASH_TRANSFER . ' THEN "Tunai + Transfer"
                WHEN status_pembayaran = ' . CASH_GIRO . ' THEN "Tunai + Giro"
                WHEN status_pembayaran = ' . KREDIT . ' THEN "Kredit"
                WHEN status_pembayaran = ' . GIRO . ' THEN "Giro"
                WHEN status_pembayaran = ' . CASH_KREDIT . ' THEN "Tunai + Kredit"
                WHEN status_pembayaran = ' . TRANSFER_KREDIT . ' THEN "Transfer + Kredit"
            ELSE "Transfer + Giro" END
            ) as status_pembayaran,
            status_hutang, jumlah_tf,jumlah_cash, jumlah_giro')
            ->leftJoin('users', 'users.id', 'sub__orders.user_id')
            ->leftJoin('admins', 'admins.id', 'sub__orders.admin_id')
            ->first();
    }

    public function totalTransaksi($awal = null, $akhir = null, $metode = null)
    {
        $transaksi = Pesanan::when($awal != null && $akhir != null, function ($query) use ($awal, $akhir) {
            $query->whereBetween('sub__orders.tanggal', [dateInputFormat($awal, 'awal'), dateInputFormat($akhir, 'akhir')]);
        })->where('admin_id', getUser()->id)
            ->whereIn('sub__orders.status', [4, 5])
            ->whereNull('deleted_at');

        if ($metode != null) {
            if ($metode == CASH) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_cash - kembalian) as total'), DB::raw('COUNT(jumlah_cash) as jumlah'))->whereIn('status_pembayaran', [CASH, CASH_GIRO, CASH_TRANSFER, CASH_KREDIT]);
            } elseif ($metode == TRANSFER) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_tf - kembalian) as total'), DB::raw('COUNT(jumlah_tf) as jumlah'))->whereIn('status_pembayaran', [TRANSFER, CASH_TRANSFER, TRANSFER_GIRO, TRANSFER_KREDIT]);
            } elseif ($metode == GIRO) {
                $transaksi->addSelect(DB::raw('SUM(jumlah_giro) as total'), DB::raw('COUNT(jumlah_giro) as jumlah'))->whereIn('status_pembayaran', [GIRO, CASH_GIRO, TRANSFER_GIRO]);
            } elseif ($metode == KREDIT) {
                $transaksi->addSelect(DB::raw('SUM(total_pesanan - kembalian) as total'), DB::raw('COUNT(total_pesanan) as jumlah'))->whereIn('status_pembayaran', [KREDIT, CASH_KREDIT, TRANSFER_KREDIT]);
            }
        } else {
            $transaksi->addSelect(DB::raw('SUM(total_pesanan) as total'), DB::raw('COUNT(*) as jumlah'));
        }

        return $transaksi->first();
    }
}
