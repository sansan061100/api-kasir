<?php

// Periode
define('HARI', 0);
define('MINGGU', 1);
define('BULAN', 2);

// Status Pembayaran
define('CASH', 0);
define('TRANSFER', 1);
define('GIRO', 2);
define('CASH_TRANSFER', 3);
define('CASH_GIRO', 4);
define('KREDIT', 5);
define('TRANSFER_GIRO', 6);
define('CASH_KREDIT', 7);
define('TRANSFER_KREDIT', 8);

// Status Hutang
define('HUTANG', 0);
define('LUNAS', 1);
