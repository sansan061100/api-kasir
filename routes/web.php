<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Models\Admin;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'v1'], function () use ($router) {
    /**
     * Auth
     */
    $router->post('/login', ['uses' => 'AuthController@login']);

    $router->group(['middleware' => 'jwt.verify'], function () use ($router) {
        /**
         * Auth
         */
        $router->get('/me', ['uses' => 'AuthController@me']);
        $router->post('/logout', ['uses' => 'AuthController@logout']);

        /**
         * Kategori
         */
        $router->get('/kategori', ['uses' => 'KategoriController@index']);

        /**
         * Barang
         */
        $router->group(['prefix' => 'barang'], function () use ($router) {
            $router->get('/', ['uses' => 'BarangController@index']);
            $router->get('/{kode}', ['uses' => 'BarangController@findByKode']);
            $router->post('/', ['uses' => 'BarangController@checkStokByKode']);
            $router->post('/harga-sales', ['uses' => 'BarangController@storeHargaSales']);
        });

        /**
         * Pelanggan
         */
        $router->group(['prefix' => 'pelanggan'], function () use ($router) {
            $router->get('/', ['uses' => 'PelangganController@index']);
            $router->post('/', ['uses' => 'PelangganController@store']);
            $router->get('/{id}', ['uses' => 'PelangganController@findById']);
            $router->get('/cek_hutang/{id}', ['uses' => 'PelangganController@cekHutang']);
            $router->get('/cek_hutang', ['uses' => 'PelangganController@cekHutangAll']);
        });

        /**
         * Pesanan
         */
        $router->group(['prefix' => 'pesanan'], function () use ($router) {
            $router->get('/', ['uses' => 'PesananController@index']);
            $router->post('/', ['uses' => 'PesananController@store']);
            $router->get('/{kode}', ['uses' => 'PesananController@findByKode']);
        });

        /**
         * Grafik
         */
        $router->group(['prefix' => 'grafik'], function () use ($router) {

            $router->get('/barang_terlaris', 'GrafikController@barangTerlaris');

            $router->get('/transaksi_terbesar', 'GrafikController@transaksiTerbesar');

            $router->get('/transaksi_per_periode', 'GrafikController@transaksiPerPeriode');
        });

        /**
         * Rekening
         */
        $router->get('/rekening', 'RekeningController@index');

        /**
         * Pembayaran
         */
        $router->get('/pembayaran', 'PembayaranController@index');
        $router->post('/pembayaran', 'PembayaranController@store');

        /**
         * Pengembalian
         */
        $router->group(['prefix' => 'pengembalian'], function () use ($router) {
            $router->get('/', 'PengembalianController@index');
            $router->post('/', 'PengembalianController@store');
            $router->get('/{kode}', 'PengembalianController@findByKode');
        });
    });
});
